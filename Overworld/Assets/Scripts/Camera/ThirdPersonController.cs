﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Camera
{
    public class ThirdPersonController : MonoBehaviour
    {
        [SerializeField]
        private string followObjectWithTag;

        [Range(0, 1f)]
        [SerializeField]
        private float interpolationSmoothness;

        private ITargetable _target;
        private ITargetablePoint _currentPoint;

        private bool CanFollow => _target != null && _target.TargetablePoints.Length > 0;

        private void Awake()
        {
            _target = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<ITargetable>();
            if (CanFollow)
            {
                _currentPoint = _target.TargetablePoints[0];
            }
        }

        private void Update()
        {
            if (CanFollow)
            {
                transform.position = Vector3.Slerp(transform.position, _currentPoint.Position, interpolationSmoothness);
                transform.LookAt(_currentPoint.TargetPosition);
            }
        }
    }
}