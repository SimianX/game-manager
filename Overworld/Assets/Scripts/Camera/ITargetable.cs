﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Camera
{
    public interface ITargetable
    {
        ITargetablePoint[] TargetablePoints { get; }

        Vector3 Position { get; }
    }
}