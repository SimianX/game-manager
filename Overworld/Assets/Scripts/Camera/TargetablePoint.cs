﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Camera
{
    public class TargetablePoint : MonoBehaviour, ITargetablePoint
    {
        private ITargetable _target;

        public Vector3 Position => transform.position;

        public Vector3 TargetPosition
        {
            get
            {
                if (_target == null)
                {
                    _target = gameObject.GetComponentInParent<ITargetable>();
                }

                return _target.Position;
            }
        }
    }
}