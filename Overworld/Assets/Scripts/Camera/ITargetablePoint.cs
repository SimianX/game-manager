﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Camera
{
    public interface ITargetablePoint
    {
        Vector3 Position { get; }
        Vector3 TargetPosition { get; }
    }
}