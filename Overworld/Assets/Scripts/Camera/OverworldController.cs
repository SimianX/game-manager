﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;

namespace Lab2.Assets.Scripts.Camera
{
    public class OverworldController : MonoBehaviour
    {
        [SerializeField]
        private string followObjectWithTag;

        [SerializeField]
        private float followHeight;

        [Range(0, 1)]
        [SerializeField]
        private float followSmoothing;

        private GameObject _followTarget;

        private void Start()
        {
            if (!_followTarget)
            {
                _followTarget = GameObject.FindGameObjectWithTag(followObjectWithTag);
            }

            transform.position = _followTarget.transform.position + new Vector3(0, followHeight, 0);
        }

        private void Update()
        {
            if (!_followTarget)
            {
                _followTarget = GameObject.FindGameObjectWithTag(followObjectWithTag);
            }

            if (_followTarget && TransitionManager.Instance.CurrentTransitionState == TransitionManager.TransitionState.TransitionComplete)
            {
                transform.position = Vector3.Slerp(transform.position, _followTarget.transform.position + new Vector3(0, followHeight, 0), followSmoothing);
            }
        }
    }
}