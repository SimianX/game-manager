﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Camera
{
    public class TargetController : MonoBehaviour, ITargetable
    {
        [SerializeField]
        private float lookSensitivity;

        [SerializeField]
        private Vector3 offset;

        [SerializeField]
        private GameObject[] counterRotation;

        public Vector3 Position => transform.position + offset;

        public ITargetablePoint[] TargetablePoints
        {
            get
            {
                if (_targetablePoints == null)
                {
                    _targetablePoints = GetComponentsInChildren<ITargetablePoint>();
                }

                return _targetablePoints;
            }
        }

        private ITargetablePoint[] _targetablePoints;

        private void Update()
        {
            Vector2 rightStickInput = new Vector2(Input.GetAxis("Look X"), Input.GetAxis("Look Y"));
            Look(rightStickInput);
        }

        private void Look(Vector2 input)
        {
            if (input.magnitude >= 0.1f)
            {
                transform.Rotate(Vector3.up * input.x * lookSensitivity * Time.deltaTime);
                //transform.Rotate(Vector3.right * input.y * lookSensitivity * Time.deltaTime);

                foreach(GameObject gameObject in counterRotation)
                {
                    gameObject.transform.Rotate(Vector3.up * -input.x * lookSensitivity * Time.deltaTime);
                    //gameObject.transform.Rotate(Vector3.right * -input.y * lookSensitivity * Time.deltaTime);
                }
            }
        }
    }
}