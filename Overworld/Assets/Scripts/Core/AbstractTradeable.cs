﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public abstract class AbstractTradeable : MonoBehaviour
    {
        private int _value;
        private bool _acquired;

        public int Value => _value;
        public bool Acquired => _acquired;

        public void SetCollectableValues(ITradeableValues tradeableValues)
        {
            _value = tradeableValues.Value;
            _acquired = tradeableValues.Acquired;
            gameObject.SetActive(!_acquired);
        }

        public void OnTriggerEnter(Collider other)
        {
            Player.Controller player = other.GetComponentInParent<Player.Controller>();

            if (player && !_acquired)
            {
                player.GainGold(_value);
                _acquired = true;
                gameObject.SetActive(!_acquired);
            }
        }

        public static TObject[] InstantiateCollectableObjects<TObject>(TObject[] instantiateFromObjects, ITradeableValues[] instantiatedObjectData, GameObject[] spawners)
                where TObject : AbstractTradeable
        {
            int spawns = spawners.Length < instantiatedObjectData.Length ? spawners.Length : instantiatedObjectData.Length;

            TObject[] spawnedObjects = new TObject[spawns];

            for (int i = 0; i < spawns; i++)
            {
                spawnedObjects[i] = Instantiate(
                    instantiateFromObjects[Random.Range(0, instantiateFromObjects.Length)],
                    spawners[i].transform.position,
                    spawners[i].transform.rotation,
                    spawners[i].transform);

                spawnedObjects[i].SetCollectableValues(instantiatedObjectData[i]);
            }

            return spawnedObjects;
        }
    }
}