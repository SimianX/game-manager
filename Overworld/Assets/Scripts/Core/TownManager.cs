﻿using System.Linq;
using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public class TownManager : MonoBehaviour
    {
        [SerializeField]
        private Vector3 miniCentre;

        [SerializeField]
        private float miniRadius;

        [SerializeField]
        private EnemyController[] enemyPrefabs;

        [SerializeField]
        private GoldController[] goldPrefabs;

        private EnemySpawner[] _monsterSpawners;
        private GoldSpawner[] _goldSpawners;

        private EnemyController[] _activeEnemies;
        private GoldController[] _activeGoldPiles;

        private bool ready;

        public TownContainer TownContainer =>
            new TownContainer(
                _activeEnemies.Select(i => i.GetEnemy()).ToArray(),
                _activeGoldPiles.Select(i => i.GetGold()).ToArray());

        private void Awake()
        {
            _monsterSpawners = GetComponentsInChildren<EnemySpawner>();
            _goldSpawners = GetComponentsInChildren<GoldSpawner>();

            ready = false;
        }

        private void Start()
        {
            _activeEnemies = AbstractDamageable.InstantiateDamageableObjects(
                enemyPrefabs,
                GameManager.Instance.GetTownEnemies(name),
                _monsterSpawners.Select(i => i.gameObject).ToArray());

            _activeGoldPiles = AbstractTradeable.InstantiateCollectableObjects(
                goldPrefabs,
                GameManager.Instance.GetTownGold(name),
                _goldSpawners.Select(i => i.gameObject).ToArray());

            ready = true;
        }

        private void OnTriggerExit(Collider other)
        {
            if (ready)
            {
                Player.Controller player = other.GetComponentInParent<Player.Controller>();

                if (player)
                {
                    GameManager.Instance.RecordTown(name, TownContainer);

                    GameManager.Instance.TranslatePlayerRelativeToExit(TransitionManager.OVERWORLD_SCENE_NAME, miniCentre, transform.position, miniRadius);
                }
            }
        }
    }
}