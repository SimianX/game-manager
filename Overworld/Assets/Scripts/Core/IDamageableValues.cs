﻿public interface IDamageableValues
{
    int CurrentHealth { get; }
    int MaxHealth { get; }
}
