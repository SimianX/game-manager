﻿using System;
using System.IO;
using System.Collections;
using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public class GameManager : MonoBehaviour
    {
        #region Serialization Strings
        public const string SAVE_FILES_BASE_DIRECTORY = "SaveFiles";
        public const string FILE_EXTENSION = ".json";

        public const string PLAYER_DATA_BASE_DIRECTORY = SAVE_FILES_BASE_DIRECTORY + "/" + "PlayerData";
        public const string PLAYER_DATA_FILE = PLAYER_DATA_BASE_DIRECTORY + "/" + "Player.json";

        public const string TOWN_DATA_BASE_DIRECTORY = SAVE_FILES_BASE_DIRECTORY + "/" + "TownData";

        public const string TOWN_NAME1 = "Town1";
        public const string TOWN_NAME2 = "Town2";
        public const string TOWN_NAME3 = "Town3";
        public const string TOWN_NAME4 = "Town4";

        public const string TOWN_DATA_FILE1 = TOWN_DATA_BASE_DIRECTORY + "/" + TOWN_NAME1 + FILE_EXTENSION;
        public const string TOWN_DATA_FILE2 = TOWN_DATA_BASE_DIRECTORY + "/" + TOWN_NAME2 + FILE_EXTENSION;
        public const string TOWN_DATA_FILE3 = TOWN_DATA_BASE_DIRECTORY + "/" + TOWN_NAME3 + FILE_EXTENSION;
        public const string TOWN_DATA_FILE4 = TOWN_DATA_BASE_DIRECTORY + "/" + TOWN_NAME4 + FILE_EXTENSION;
        #endregion

        public static GameManager Instance { get; private set; }
        public bool Ready { get; private set; }

        public bool CanContinue { get; private set; }

        public PlayerContainer PlayerContainer => _playerContainer;
        public Player.Controller Player => FindObjectOfType<Player.Controller>();

        private bool IsTownSceneActive =>
            Player.loadedScene == TOWN_NAME1 ||
            Player.loadedScene == TOWN_NAME2 ||
            Player.loadedScene == TOWN_NAME3 ||
            Player.loadedScene == TOWN_NAME4;

        private TownContainer GetActiveTown =>
            !IsTownSceneActive ? null :
            FindObjectOfType<TownManager>().TownContainer;

        private PlayerContainer _playerContainer;
        private TownContainer _townContainer1;
        private TownContainer _townContainer2;
        private TownContainer _townContainer3;
        private TownContainer _townContainer4;

        private bool _movingPlayer;

        public event EventHandler OnNewGame;
        public event EventHandler OnRest;
        public event EventHandler OnLoad;
        public event EventHandler OnSave;

        // Instantiate singleton and set defaults
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(this);
            }

            Ready = false;
        }

        // Get ready for initial load
        private IEnumerator Start()
        {
            yield return StartCoroutine(LoadData());
            Ready = true;
        }

        #region Town
        public Enemy[] GetTownEnemies(string townName)
        {
            Enemy[] townEnemies;

            switch (townName)
            {
                case TOWN_NAME1:
                    townEnemies = _townContainer1.enemies;
                    break;
                case TOWN_NAME2:
                    townEnemies = _townContainer2.enemies;
                    break;
                case TOWN_NAME3:
                    townEnemies = _townContainer3.enemies;
                    break;
                case TOWN_NAME4:
                    townEnemies = _townContainer4.enemies;
                    break;
                default:
                    townEnemies = new Enemy[0];
                    break;
            }

            return townEnemies;
        }

        public Gold[] GetTownGold(string townName)
        {
            Gold[] townGold;

            switch (townName)
            {
                case TOWN_NAME1:
                    townGold = _townContainer1.goldPiles;
                    break;
                case TOWN_NAME2:
                    townGold = _townContainer2.goldPiles;
                    break;
                case TOWN_NAME3:
                    townGold = _townContainer3.goldPiles;
                    break;
                case TOWN_NAME4:
                    townGold = _townContainer4.goldPiles;
                    break;
                default:
                    townGold = new Gold[0];
                    break;
            }

            return townGold;
        }
        #endregion

        #region Loading
        public IEnumerator Continue()
        {
            if (CanContinue)
            {
                yield return StartCoroutine(TransitionManager.Instance.AddScene(TransitionManager.PLAYER_SCENE_NAME));
                Player.SetPlayerFromContainer(PlayerContainer);
                TransitionManager.Instance.StartTransition(_playerContainer.loadedScene);
            }
        }

        public IEnumerator NewGame()
        {
            _playerContainer = new PlayerContainer();
            _townContainer1 = new TownContainer();
            _townContainer2 = new TownContainer();
            _townContainer3 = new TownContainer();
            _townContainer4 = new TownContainer();

            OnNewGame?.Invoke(this, EventArgs.Empty);

            yield return StartCoroutine(TransitionManager.Instance.AddScene(TransitionManager.PLAYER_SCENE_NAME));
            Player.SetPlayerFromContainer(PlayerContainer);
            TransitionManager.Instance.StartTransition(_playerContainer.loadedScene);
        }

        public IEnumerator Restart()
        {
            yield return Continue();
        }

        public IEnumerator SaveAndReturnToTitle()
        {
            RecordContainers();
            yield return StartCoroutine(SaveData());
            TransitionManager.Instance.StartTransition(TransitionManager.MENU_SCENE_NAME);
        }

        public IEnumerator SaveAndQuit()
        {
            RecordContainers();
            yield return StartCoroutine(SaveData());
            Application.Quit();
        }

        private void RecordContainers()
        {
            RecordPlayer(Player);
            if (IsTownSceneActive)
            {
                RecordTown(Player.loadedScene, GetActiveTown);
            }
            CanContinue = true;
        }

        public void TranslatePlayerRelativeToExit(string newSceneName, Vector3 newPosition, Vector3 exitPivot, float newRadius)
        {
            if (Player.Set)
            {
                StartCoroutine(MovePlayer(newSceneName, newPosition, exitPivot, newRadius));
            }
        }

        private IEnumerator MovePlayer(string newSceneName, Vector3 newPosition, Vector3 exitPivot, float newRadius)
        {
            if (!_movingPlayer)
            {
                _movingPlayer = true;
                TransitionManager.Instance.StartTransition(newSceneName);
                while (!TransitionManager.Instance.Ready)
                {
                    yield return null;
                }
                Player.transform.position = newPosition + (Player.transform.position - exitPivot).normalized * newRadius;
                yield return new WaitForSeconds(1.25f); // TODO: Replace with constant
                _movingPlayer = false;
            }
        }

        public void RecordTown(string townName, TownContainer town)
        {
            switch (townName)
            {
                case TOWN_NAME1:
                    _townContainer1 = town;
                    break;
                case TOWN_NAME2:
                    _townContainer2 = town;
                    break;
                case TOWN_NAME3:
                    _townContainer3 = town;
                    break;
                case TOWN_NAME4:
                    _townContainer4 = town;
                    break;
                default:
                    break;
            }
        }

        private void RecordPlayer(Player.Controller player)
        {
            _playerContainer.posX = player.transform.position.x;
            _playerContainer.posY = player.transform.position.y;
            _playerContainer.posZ = player.transform.position.z;

            _playerContainer.rotx = player.transform.rotation.x;
            _playerContainer.roty = player.transform.rotation.y;
            _playerContainer.rotz = player.transform.rotation.z;
            _playerContainer.rotw = player.transform.rotation.w;

            _playerContainer.health = player.CurrentHealth;
            _playerContainer.maxHealth = player.MaxHealth;

            _playerContainer.heldGold = player.heldGold;
            _playerContainer.loadedScene = player.loadedScene;
        }

        public IEnumerator ResetAllEnemies(bool save)
        {
            Enemy[] enemies = new Enemy[2];
            for(int i = 0; i < enemies.Length; i++)
            {
                enemies[i] = new Enemy(3, 3);
            }

            _townContainer2.enemies = enemies;
            _townContainer3.enemies = enemies;
            _townContainer4.enemies = enemies;

            RecordPlayer(Player);

            if (save)
            {
                yield return StartCoroutine(SaveData());
                OnRest?.Invoke(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Serialization
        private IEnumerator SaveData()
        {
            yield return ValidateFileStructure();
            SaveJSON(PLAYER_DATA_FILE, _playerContainer);
            SaveJSON(TOWN_DATA_FILE1, _townContainer1);
            SaveJSON(TOWN_DATA_FILE2, _townContainer2);
            SaveJSON(TOWN_DATA_FILE3, _townContainer3);
            SaveJSON(TOWN_DATA_FILE4, _townContainer4);

            OnSave?.Invoke(this, EventArgs.Empty);
        }

        private IEnumerator LoadData()
        {
            yield return ValidateFileStructure();
            CanContinue = LoadJSON(PLAYER_DATA_FILE, ref _playerContainer);
            CanContinue = LoadJSON(TOWN_DATA_FILE1, ref _townContainer1);
            CanContinue = LoadJSON(TOWN_DATA_FILE2, ref _townContainer2);
            CanContinue = LoadJSON(TOWN_DATA_FILE3, ref _townContainer3);
            CanContinue = LoadJSON(TOWN_DATA_FILE4, ref _townContainer4);

            OnLoad?.Invoke(this, EventArgs.Empty);
        }

        private static void SaveJSON<TSerializable>(string path, TSerializable container) where TSerializable : class, new()
        {
            File.WriteAllText(path, JsonUtility.ToJson(container));
        }

        private static bool LoadJSON<TSerializable>(string path, ref TSerializable container) where TSerializable : class, new()
        {
            string json = File.ReadAllText(path);

            if (!File.Exists(path) || string.IsNullOrEmpty(json))
            {
                container = new TSerializable();
                return false;
            }
            else
            {
                try
                {
                    container = JsonUtility.FromJson<TSerializable>(json);
                    return true;
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e.Message);

                    container = new TSerializable();
                    return false;
                }
            }
        }
        #endregion

        #region Validation
        /// <summary>
        /// Validates SaveFiles's File System Hierarchy (FSH).
        /// If a file is missing, an empty file will be made in it's place.
        /// </summary>
        /// <returns>
        /// Will yield until the FSH is validated
        /// </returns>
        private IEnumerator ValidateFileStructure()
        {
            // Validate Directories
            yield return StartCoroutine(ValidateDirectory(SAVE_FILES_BASE_DIRECTORY));
            yield return StartCoroutine(ValidateDirectory(PLAYER_DATA_BASE_DIRECTORY));
            yield return StartCoroutine(ValidateDirectory(TOWN_DATA_BASE_DIRECTORY));

            // Validate Files
            yield return StartCoroutine(ValidateFile(PLAYER_DATA_FILE));
            yield return StartCoroutine(ValidateFile(TOWN_DATA_FILE1));
            yield return StartCoroutine(ValidateFile(TOWN_DATA_FILE2));
            yield return StartCoroutine(ValidateFile(TOWN_DATA_FILE3));
            yield return StartCoroutine(ValidateFile(TOWN_DATA_FILE4));
        }

        /// <summary>
        /// Validates a directory given a path string
        /// </summary>
        /// <param name="path">
        /// Path to the directory
        /// </param>
        /// <returns>
        /// Will yield until the directory is validated
        /// </returns>
        private IEnumerator ValidateDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.SetLastAccessTimeUtc(path, System.DateTime.UtcNow);
            }
            else
            {
                Directory.CreateDirectory(path);
                yield return ValidateDirectory(path);
            }
        }

        /// <summary>
        /// Validates a file given a path string
        /// </summary>
        /// <param name="path">
        /// Path to the file
        /// </param>
        /// <returns>
        /// Will yield until the file is validated
        /// </returns>
        private IEnumerator ValidateFile(string path)
        {
            if (File.Exists(path))
            {
                File.SetLastAccessTimeUtc(path, System.DateTime.UtcNow);
            }
            else
            {
                FileStream stream = File.Create(path);
                stream.Close();
                yield return ValidateFile(path);
            }
        }
        #endregion
    }
}