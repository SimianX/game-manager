﻿using System.Collections;
using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public class GameLoadingManager : MonoBehaviour
    {
        private IEnumerator Start()
        {
            while (
                !GameManager.Instance.Ready ||
                !TransitionManager.Instance.Ready ||
                !Inventory.InventoryManager.Instance.Ready ||
                !AlertManager.Instance.Ready)
            {
                yield return null;
            }
            TransitionManager.Instance.StartTransition(TransitionManager.MENU_SCENE_NAME);
        }
    }
}