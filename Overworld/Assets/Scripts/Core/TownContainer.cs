﻿namespace Lab2.Assets.Scripts.Core
{
    [System.Serializable]
    public class TownContainer
    {
        public Enemy[] enemies;
        public Gold[] goldPiles;

        public TownContainer()
        {
            enemies = new Enemy[2];

            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i] = new Enemy();
            }

            goldPiles = new Gold[3];

            for (int i = 0; i < goldPiles.Length; i++)
            {
                goldPiles[i] = new Gold();
            }
        }

        public TownContainer(Enemy[] enemies, Gold[] goldPiles)
        {
            this.enemies = enemies;
            this.goldPiles = goldPiles;
        }
    }
}