﻿using System;
using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public abstract class AbstractDamageable : MonoBehaviour
    {
        public int CurrentHealth => currentHealth;
        public int MaxHealth => maxHealth;

        protected int currentHealth;
        protected int maxHealth;

        public event EventHandler<DamageArgs> OnDamage;
        public class DamageArgs : EventArgs
        {
            public int damage;
            public int startingHealth;
            public int resultingHealth;
            public int maxHealth;
            public int healthLost;
        }

        public event EventHandler<HealArgs> OnHeal;
        public class HealArgs : EventArgs
        {
            public int heal;
            public int startingHealth;
            public int resultingHealth;
            public int maxHealth;
            public int healthGained;
        }

        public event EventHandler OnDeath;

        protected abstract void Die();

        public void DealDamage(int damage)
        {
            if (damage >= 0)
            {
                int startingHealth = currentHealth;

                currentHealth -= damage;

                if (currentHealth < 0)
                {
                    currentHealth = 0;
                }

                if (currentHealth == 0)
                {
                    Die();
                    OnDeath?.Invoke(this, EventArgs.Empty);
                }

                OnDamage?.Invoke(this, new DamageArgs
                {
                    damage = damage,
                    startingHealth = startingHealth,
                    resultingHealth = currentHealth,
                    maxHealth = maxHealth,
                    healthLost = startingHealth - currentHealth
                });
            }
        }

        public void HealDamage(int heal)
        {
            if (heal >= 0)
            {
                int startinghealth = currentHealth;

                currentHealth += heal;

                if (currentHealth > maxHealth)
                {
                    currentHealth = maxHealth;
                }

                OnHeal?.Invoke(this, new HealArgs
                {
                    heal = heal,
                    startingHealth = startinghealth,
                    resultingHealth = currentHealth,
                    maxHealth = maxHealth,
                    healthGained = currentHealth - startinghealth
                });
            }
        }

        public void SetDamageableValues(IDamageableValues damageableValues)
        {
            SetDamageableValues(damageableValues.CurrentHealth, damageableValues.MaxHealth);
        }

        public void SetDamageableValues(int currentHealth, int maxHealth)
        {
            this.currentHealth = currentHealth;
            this.maxHealth = maxHealth;

            if (currentHealth == 0)
            {
                Die();
            }
        }

        

        public static TObject[] InstantiateDamageableObjects<TObject>(TObject[] instantiateFromObjects, IDamageableValues[] instantiatedObjectData, GameObject[] spawners)
                where TObject : AbstractDamageable
        {
            int spawns = spawners.Length < instantiatedObjectData.Length ? spawners.Length : instantiatedObjectData.Length;

            TObject[] spawnedObjects = new TObject[spawns];

            for (int i = 0; i < spawns; i++)
            {
                spawnedObjects[i] = Instantiate(
                    instantiateFromObjects[UnityEngine.Random.Range(0, instantiateFromObjects.Length)],
                    spawners[i].transform.position,
                    spawners[i].transform.rotation,
                    spawners[i].transform);

                spawnedObjects[i].SetDamageableValues(instantiatedObjectData[i]);
            }

            return spawnedObjects;
        }
    }
}