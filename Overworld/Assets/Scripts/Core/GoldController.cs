﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public class GoldController : AbstractTradeable
    {
        [SerializeField]
        private float rotationSpeed;

        private void Update()
        {
            transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
        }
    }

    public static class GoldExtension
    {
        public static Gold GetGold(this GoldController goldController)
        {
            return new Gold(goldController.Value, goldController.Acquired);
        }
    }
}