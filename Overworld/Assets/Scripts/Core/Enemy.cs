﻿namespace Lab2.Assets.Scripts.Core
{
    [System.Serializable]
    public class Enemy : IDamageableValues
    {
        public int currentHealth;
        public int maxHealth;

        public int CurrentHealth => currentHealth;
        public int MaxHealth => maxHealth;

        public Enemy()
        {
            currentHealth = 3;
            maxHealth = 3;
        }

        public Enemy(int currentHealth, int maxHealth)
        {
            this.currentHealth = currentHealth;
            this.maxHealth = maxHealth;
        }
    }
}