﻿namespace Lab2.Assets.Scripts.Core
{
    public interface ITradeableValues
    {
        int Value { get; }
        bool Acquired { get; }
    }
}