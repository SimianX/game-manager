﻿public interface ITradeable
{
    int Value { get; }
    bool Acquired { get; }
}
