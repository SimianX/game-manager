﻿namespace Lab2.Assets.Scripts.Core
{
    [System.Serializable]
    public class Gold : ITradeableValues
    {
        public int value;
        public bool acquired;

        public int Value => value;
        public bool Acquired => acquired;

        public Gold()
        {
            value = 10;
            acquired = false;
        }

        public Gold(int value, bool acquired)
        {
            this.value = value;
            this.acquired = acquired;
        }
    }
}