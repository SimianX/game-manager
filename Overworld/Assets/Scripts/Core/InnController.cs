﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public class InnController : MonoBehaviour
    {
        private void Start()
        {
            
        }

        private void OnTriggerEnter(Collider other)
        {
            IRestable restable = other.GetComponentInParent<IRestable>();
            if (restable != null)
            {
                restable.NearBed = true;
                restable.RestCost = 10;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            IRestable restable = other.GetComponentInParent<IRestable>();
            if (restable != null)
            {
                restable.NearBed = false;
            }
        }
    }
}