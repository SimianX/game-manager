﻿namespace Lab2.Assets.Scripts.Core
{
    public interface IInteractable
    {
        bool Use { get; }
    }
}