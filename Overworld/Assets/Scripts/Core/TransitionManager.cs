﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Lab2.Assets.Scripts.Core
{
    public class TransitionManager : MonoBehaviour
    {
        public const float TRANSITION_TIME = 0.5f;

        public const string DEFAULT_SCENE = OVERWORLD_SCENE_NAME;
        public const string PLAYER_SCENE_NAME = "Player";
        public const string OVERWORLD_SCENE_NAME = "OverworldScene";
        public const string TRANSITION_SCENE_NAME = "TransitionScene";
        public const string MENU_SCENE_NAME = "MenuScene";
        public const string LOADING_SCENE = "LoadingScene";

        public static TransitionManager Instance { get; private set; }
        public bool Ready { get; private set; }

        public string ActiveSceneName => SceneManager.GetActiveScene().name;

        public enum PlayerLoadType
        {
            NewGame,
            Continue,
            AreaTransition
        }

        public enum TransitionState
        {
            TransitionComplete,
            StartingTransition,
            EndingTransition
        }

        public TransitionState CurrentTransitionState { get; private set; }

        // Transition Events
        public event EventHandler<TransitionArgs> OnTransition;
        public class TransitionArgs : EventArgs
        {
            public TransitionState transitionState;
            public Scene transitionedScene;
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(Instance);
            }
            else if (Instance != this)
            {
                Destroy(this);
            }

            CurrentTransitionState = TransitionState.TransitionComplete;
            Ready = false;
        }

        private IEnumerator Start()
        {
            yield return StartCoroutine(ValidateScene(TRANSITION_SCENE_NAME));
            Ready = true;
        }

        public void StartTransition(string newSceneName)
        {
            StartCoroutine(TransitionLoad(newSceneName));
        }

        private IEnumerator TransitionLoad(string newSceneName)
        {
            // Check that there are no ongoing transitions
            if (CurrentTransitionState == TransitionState.TransitionComplete)
            {
                CurrentTransitionState = TransitionState.StartingTransition; // Prevent concurrent loads

                Scene oldScene = SceneManager.GetActiveScene(); // Get Old Scene

                yield return StartCoroutine(ValidateScene(TRANSITION_SCENE_NAME)); // Ensure Transition Exists

                OnTransition?.Invoke(this, new TransitionArgs // Signal Transition Start
                {
                    transitionState = CurrentTransitionState,
                    transitionedScene = SceneManager.GetActiveScene()
                });

                yield return new WaitForSeconds(TRANSITION_TIME); // Wait for fade-in

                // Check if reloading the same scene
                if (oldScene.name != newSceneName)
                {
                    // Load new scene
                    AsyncOperation newSceneAsync = SceneManager.LoadSceneAsync(newSceneName, LoadSceneMode.Additive);
                    while (!newSceneAsync.isDone)
                    {
                        yield return null;
                    }
                }

                // Check if the new scene is valid
                Scene newScene = SceneManager.GetSceneByName(newSceneName);
                if (newScene.IsValid())
                {
                    if (oldScene != newScene) // Accommodate reloads
                    {   
                        SceneManager.UnloadSceneAsync(oldScene); // Unload Old Scene
                    }

                    SceneManager.SetActiveScene(newScene); // Set new scene to active
                }
                else
                {
                    Debug.LogError("New Scene is invalid! Did you enter the correct name?");
                }

                // End Transition
                CurrentTransitionState = TransitionState.EndingTransition;
                OnTransition?.Invoke(this, new TransitionArgs
                {
                    transitionState = CurrentTransitionState,
                    transitionedScene = SceneManager.GetActiveScene()
                });

                yield return new WaitForSeconds(TRANSITION_TIME); // Wait for fade-out

                // Signal Completion
                CurrentTransitionState = TransitionState.TransitionComplete; 
                OnTransition?.Invoke(this, new TransitionArgs
                {
                    transitionState = CurrentTransitionState,
                    transitionedScene = SceneManager.GetActiveScene()
                });
            }
        }

        public IEnumerator AddScene(string sceneName)
        {
            yield return StartCoroutine(ValidateScene(sceneName));
        }

        private IEnumerator ValidateScene(string sceneName)
        {
            if (!SceneManager.GetSceneByName(sceneName).IsValid())
            {
                AsyncOperation transitionAsync = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

                // Wait for Transition Scene
                while (!transitionAsync.isDone)
                {
                    yield return null;
                }
            }
        }
    }
}