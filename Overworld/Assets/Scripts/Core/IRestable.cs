﻿namespace Lab2.Assets.Scripts.Core
{
    public interface IRestable
    {
        bool NearBed { get; set; }
        int RestCost { get; set; }
    }
}