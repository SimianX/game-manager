﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public class MiniTownController : MonoBehaviour
    {
        [SerializeField]
        private string townName;

        [SerializeField]
        private Vector3 realTownPosition;

        [SerializeField]
        private float realTownRadius;

        private bool ready;

        private void Start()
        {
            ready = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (ready)
            {
                Player.Controller player = other.GetComponentInParent<Player.Controller>();

                if (player)
                {
                    GameManager.Instance.TranslatePlayerRelativeToExit(townName, realTownPosition, transform.position, realTownRadius);
                }
            }   
        }
    }
}