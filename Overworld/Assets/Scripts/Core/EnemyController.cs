﻿using Lab2.Assets.Scripts.Inventory;
using Lab2.Assets.Scripts.Player;
using UnityEngine;
using UnityEngine.AI;

namespace Lab2.Assets.Scripts.Core
{
    [RequireComponent(typeof(NavMeshAgent), typeof(EnemyInventory))]
    public class EnemyController : AbstractDamageable
    {
        private const string TRIGGER_WALKING_KEY = "StartWalking";
        private const string TRIGGER_IDLE_KEY = "StopWalking";
        private const string TRIGGER_ATTACK_KEY = "Attack";
        private const string TRIGGER_DEAD_KEY = "Dead";

        private const float AttackDistance = 2f;
        private const float DetectionDistance = 5f;
        private const float WalkingDeadzone = 0.1f;

        [SerializeField]
        private Animator anim;

        [SerializeField]
        private int damage;

        [SerializeField]
        private float attackCooldown;

        [SerializeField]
        private HurtboxController hurtbox;

        private NavMeshAgent _agent;
        private Controller _target;
        private EnemyInventory _enemyInventory;
        private float _attackTimestamp;
        private bool _ready;

        private enum EnemyState
        {
            Idle,
            Walking,
            Attack,
            Dead
        }

        private EnemyState _currentEnemyState;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _attackTimestamp = Time.time - attackCooldown;
        }

        private void Start()
        {
            _target = null;
            _currentEnemyState = EnemyState.Idle;
            _enemyInventory = GetComponent<EnemyInventory>();
            _enemyInventory.key = "EnemyInstanceID_" + gameObject.GetInstanceID().ToString();
        }

        private void Update()
        {
            if (currentHealth == 0 && _currentEnemyState != EnemyState.Dead && _ready)
            {
                Die();
            }

            if (_currentEnemyState != EnemyState.Dead)
            {
                // Check if attack is finished
                if (_currentEnemyState == EnemyState.Attack)
                {
                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName(TRIGGER_ATTACK_KEY))
                    {
                        Idle();
                    }
                }
                else
                {
                    if (_agent.velocity.magnitude > WalkingDeadzone && _currentEnemyState == EnemyState.Idle)
                    {
                        Walk();
                    }
                    else if (_agent.velocity.magnitude <= WalkingDeadzone && _currentEnemyState == EnemyState.Walking)
                    {
                        Idle();
                    }

                    // Search for target
                    if (_target == null)
                    {
                        Player.Controller[] players = FindObjectsOfType<Player.Controller>();
                        foreach (Player.Controller player in players)
                        {
                            if (Vector3.Distance(transform.position, player.transform.position) <= DetectionDistance)
                            {
                                _target = player;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (Time.time >= _attackTimestamp + attackCooldown &&
                            Vector3.Distance(transform.position, _target.transform.position) <= AttackDistance)
                        {
                            Attack();
                        }
                    }

                    // Agent Destination
                    if (_currentEnemyState == EnemyState.Attack ||
                        _currentEnemyState == EnemyState.Dead)
                    {
                        _agent.destination = transform.position;
                        _agent.stoppingDistance = 0;
                    }
                    else
                    {
                        if (_target != null)
                        {
                            _agent.destination = _target.transform.position;
                            _agent.stoppingDistance = AttackDistance/2;
                        }
                        else
                        {
                            _agent.destination = transform.position;
                            _agent.stoppingDistance = 0;
                        }
                    }
                }
            }

            if (CurrentHealth != 0 && !_ready)
            {
                _ready = true;
            }
        }

        protected override void Die()
        {
            _currentEnemyState = EnemyState.Dead;
            anim.SetTrigger(TRIGGER_DEAD_KEY);
            _agent.enabled = false;

            if (_enemyInventory is object)
            {
                InventoryContainer.Drop(Random.Range(0, _enemyInventory.Container.HeldItems.Length), _enemyInventory.Container);
            }
        }

        private void Idle()
        {
            _currentEnemyState = EnemyState.Idle;
            anim.SetTrigger(TRIGGER_IDLE_KEY);
            _agent.enabled = true;
        }

        private void Walk()
        {
            _currentEnemyState = EnemyState.Walking;
            anim.SetTrigger(TRIGGER_WALKING_KEY);
            _agent.enabled = true;
        }

        private void Attack()
        {
            _attackTimestamp = Time.time;
            _currentEnemyState = EnemyState.Attack;
            hurtbox.RefreshHits(1, damage);
            anim.SetTrigger(TRIGGER_ATTACK_KEY);
            _agent.enabled = true;
        }
    }

    public static class EnemyExtension
    {
        public static Enemy GetEnemy(this EnemyController enemyController)
        {
            return new Enemy(enemyController.CurrentHealth, enemyController.MaxHealth);
        }
    }
}