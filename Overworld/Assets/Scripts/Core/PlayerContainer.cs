﻿namespace Lab2.Assets.Scripts.Core
{
    [System.Serializable]
    public class PlayerContainer
    {
        public const int DEFAULT_HEALTH = 12;

        public float posX;
        public float posY;
        public float posZ;

        public float rotx;
        public float roty;
        public float rotz;
        public float rotw;

        public int health;
        public int maxHealth;

        public int heldGold;

        public string loadedScene;

        public PlayerContainer()
        {
            posX = 0f;
            posY = 0f;
            posZ = 0f;

            rotx = 0f;
            roty = 0f;
            rotz = 0f;
            rotw = 0f;

            health = DEFAULT_HEALTH;
            maxHealth = DEFAULT_HEALTH;

            heldGold = 0;

            loadedScene = TransitionManager.DEFAULT_SCENE;
        }
    }
}