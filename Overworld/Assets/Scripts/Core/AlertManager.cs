﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lab2.Assets.Scripts.Core
{
    public class AlertManager : MonoBehaviour
    {
        public static AlertManager Instance { get; private set; }
        public bool Ready { get; private set; }

        public event EventHandler<UserErrorArgs> OnUserError;
        public class UserErrorArgs
        {
            public string message;
        }

        // Instantiate singleton and set defaults
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(this);
            }

            Ready = false;
        }

        // Get ready for initial load
        private void Start()
        {
            Ready = true;
        }

        public void LogUserError(string message)
        {
            OnUserError?.Invoke(this, new UserErrorArgs() { message = message });
        }
    }
}