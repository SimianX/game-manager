﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;

namespace Lab2.Assets.Scripts.Player
{
    public class RestController : MonoBehaviour, IRestable
    {
        public bool NearBed { get; set; }
        public int RestCost { get; set; }
    }
}