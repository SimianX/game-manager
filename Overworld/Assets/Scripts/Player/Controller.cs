﻿using Lab2.Assets.Scripts.Core;
using System;
using UnityEngine;

namespace Lab2.Assets.Scripts.Player
{
    public class Controller : AbstractDamageable, IInteractable
    {
        private const int MaxGold = 999;

        [SerializeField]
        private float movementSpeed;

        [Range(0, 1)]
        [SerializeField]
        private float deadzone;

        [SerializeField]
        private Animator model;

        [SerializeField]
        private HurtboxController hurtbox;

        [SerializeField]
        private int damage;

        public int heldGold;
        public string loadedScene;

        public bool Use { get; private set; }
        public bool Set { get; private set; }

        public event EventHandler<SetArgs> OnSet;
        public class SetArgs : EventArgs
        {
            public int heldGold;
            public int currentHealth;
            public int maxHealth;
        }

        public enum PlayerState
        {
            Idle,
            Running,
            Attack,
            Dead
        }

        public PlayerState CurrentPlayerState { get; private set; }

        public event EventHandler<GoldGainedArgs> OnGoldGained;
        public class GoldGainedArgs : EventArgs
        {
            public int initialGain;
            public int startingAmount;
            public int resultingAmount;
            public int goldGained;
        }

        public event EventHandler<GoldLostArgs> OnGoldLost;
        public class GoldLostArgs : EventArgs
        {
            public int initialCost;
            public int startingAmount;
            public int resultingAmount;
            public int goldLost;
        }

        private void Awake()
        {
            Set = false;
        }

        private void OnEnable()
        {
            TransitionManager.Instance.OnTransition += Instance_OnTransition;
        }

        private void OnDisable()
        {
            TransitionManager.Instance.OnTransition -= Instance_OnTransition;
        }

        private void Update()
        {
            if (Set)
            {
                if (currentHealth == 0 && CurrentPlayerState != PlayerState.Dead)
                {
                    Die();
                }

                if (CurrentPlayerState != PlayerState.Dead)
                {
                    if (CurrentPlayerState == PlayerState.Attack && !model.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                    {
                        CurrentPlayerState = PlayerState.Idle;
                        hurtbox.ClearHits();
                    }

                    Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

                    // Moving
                    if (input.magnitude > deadzone)
                    {
                        Move(input);
                    }

                    // Using
                    if (Input.GetButtonDown("Use"))
                    {
                        Use = true;
                    }
                    else
                    {
                        Use = false;
                    }

                    // State machine
                    if ((CurrentPlayerState == PlayerState.Idle || CurrentPlayerState == PlayerState.Running) && Input.GetButtonDown("Fire1"))
                    {
                        Attack();
                    }
                    else if (CurrentPlayerState == PlayerState.Idle && input.magnitude > deadzone)
                    {
                        Run();
                    }
                    else if (CurrentPlayerState == PlayerState.Running && input.magnitude <= deadzone)
                    {
                        Idle();
                    }
                }
            }
        }

        private void Instance_OnTransition(object sender, TransitionManager.TransitionArgs e)
        {
            if (e.transitionState == TransitionManager.TransitionState.TransitionComplete &&
                e.transitionedScene.name != TransitionManager.LOADING_SCENE &&
                e.transitionedScene.name != TransitionManager.MENU_SCENE_NAME &&
                e.transitionedScene.name != TransitionManager.TRANSITION_SCENE_NAME)
            {
                loadedScene = e.transitionedScene.name;
            }
        }

        public void SetPlayerFromContainer(PlayerContainer playerContainer)
        {
            transform.position = new Vector3(playerContainer.posX, playerContainer.posY, playerContainer.posZ);
            transform.rotation = new Quaternion(playerContainer.rotx, playerContainer.roty, playerContainer.rotz, playerContainer.rotw);
            currentHealth = playerContainer.health;
            maxHealth = playerContainer.maxHealth;
            heldGold = playerContainer.heldGold;

            Idle();
            OnSet?.Invoke(this, new SetArgs
            {
                heldGold = heldGold,
                currentHealth = CurrentHealth,
                maxHealth = MaxHealth
            });
            Set = true;
        }

        protected override void Die()
        {
            CurrentPlayerState = PlayerState.Dead;
            model.SetTrigger("StartDead");
        }

        private void Attack()
        {
            CurrentPlayerState = PlayerState.Attack;
            model.SetTrigger("StartAttack");
            hurtbox.RefreshHits(1, damage);
        }

        private void Run()
        {
            CurrentPlayerState = PlayerState.Running;
            model.SetTrigger("StartRunning");
        }

        private void Idle()
        {
            CurrentPlayerState = PlayerState.Idle;
            model.SetTrigger("StartIdle");
        }

        private void Move(Vector2 input)
        {
            if (loadedScene == TransitionManager.OVERWORLD_SCENE_NAME)
            {
                transform.forward = Vector3.forward;
            }

            model.transform.forward = new Vector3(input.x, 0, input.y);
            transform.Translate(model.transform.forward * movementSpeed * Time.deltaTime, Space.Self);
            model.transform.localEulerAngles = new Vector3(0, model.transform.localEulerAngles.y + transform.localEulerAngles.y, 0);
        }

        #region Gold
        public void GainGold(int gain)
        {
            if (gain >= 0)
            {
                int startingAmount = heldGold;

                heldGold += gain;

                if (heldGold > MaxGold)
                {
                    heldGold = MaxGold;
                }

                OnGoldGained?.Invoke(this, new GoldGainedArgs
                {
                    initialGain = gain,
                    startingAmount = startingAmount,
                    resultingAmount = heldGold,
                    goldGained = heldGold - startingAmount
                });
            }
        }

        public void LoseGold(int cost)
        {
            if (cost >= 0)
            {
                int startingAmount = heldGold;

                heldGold -= cost;

                if (heldGold < 0)
                {
                    heldGold = 0;
                }

                OnGoldLost?.Invoke(this, new GoldLostArgs
                {
                    initialCost = cost,
                    startingAmount = startingAmount,
                    resultingAmount = heldGold,
                    goldLost = startingAmount - heldGold
                });
            }
        }
        #endregion
    }
}