﻿using Lab2.Assets.Scripts.Inventory;
using UnityEngine;

namespace Lab2.Assets.Scripts.Player
{
    [RequireComponent(typeof(Controller))]
    public class BoxFinderController : MonoBehaviour
    {
        [SerializeField]
        private float openingDistance;

        public bool OpeningBox { get; private set; }
        public ItemBoxInventory LastBox { get; private set; }

        public ItemBoxInventory[] AllLoadedItemBoxes => FindObjectsOfType<ItemBoxInventory>();
        
        public bool CanOpenBox => NearestItemBoxDistance <= openingDistance;

        private ItemBoxInventory NearestItemBox
        {
            get
            {
                ItemBoxInventory nearestItemBox = null;
                float nearestDistance = float.MaxValue;

                for (int i = 0; i < AllLoadedItemBoxes.Length; i++)
                {
                    float currentDistance = Vector3.Distance(transform.position, AllLoadedItemBoxes[i].transform.position);

                    if (currentDistance < nearestDistance)
                    {
                        nearestItemBox = AllLoadedItemBoxes[i];
                        nearestDistance = currentDistance;
                    }
                }

                return nearestItemBox;
            }
        }

        public float NearestItemBoxDistance
        {
            get
            {
                float nearestDistance = float.MaxValue;

                for (int i = 0; i < AllLoadedItemBoxes.Length; i++)
                {
                    float currentDistance = Vector3.Distance(transform.position, AllLoadedItemBoxes[i].transform.position);

                    if (currentDistance < nearestDistance)
                    {
                        nearestDistance = currentDistance;
                    }
                }

                return nearestDistance;
            }
        }

        private void Update()
        {
            OpeningBox = Input.GetButtonDown("Use") && CanOpenBox;
            if (OpeningBox)
            {
                LastBox = NearestItemBox;
            }
        }
    }
}