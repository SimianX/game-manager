﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;

namespace Lab2.Assets.Scripts.Player
{
    public class HurtboxController : MonoBehaviour
    {
        private int _hits;
        private int _damagePerHit;

        private void OnTriggerStay(Collider collider)
        {
            if (_hits > 0)
            {
                AbstractDamageable damageable = collider.GetComponentInParent<AbstractDamageable>();

                if (damageable != null && damageable.transform.root != transform.root)
                {
                    damageable.DealDamage(_damagePerHit);
                    _hits--;
                }
            }
        }

        public void RefreshHits(int hits, int damagePerHit)
        {
            _hits = hits;
            _damagePerHit = damagePerHit;
        }

        public void ClearHits()
        {
            _hits = 0;
        }
    }
}