﻿using Lab2.Assets.Scripts.Player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI.Inventory
{
    public class OpenBoxPrompt : MonoBehaviour
    {
        [SerializeField]
        private Image buttonPrompt;

        [SerializeField]
        private TextMeshProUGUI message;

        [SerializeField]
        private Sprite openBoxPrompt;

        [SerializeField]
        private Sprite openInventoryPrompt;

        [SerializeField]
        private string openBoxMessage;
        
        [SerializeField]
        private string openInventoryMessage;

        private BoxFinderController boxFinder;

        private void Awake()
        {
            boxFinder = transform.root.GetComponent<BoxFinderController>();
        }

        private void Update()
        {
            bool nearBox = boxFinder.CanOpenBox;
            buttonPrompt.sprite = nearBox ? openBoxPrompt : openInventoryPrompt;
            message.text = nearBox ? openBoxMessage : openInventoryMessage;
        }
    }
}