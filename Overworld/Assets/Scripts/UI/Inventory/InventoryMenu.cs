﻿using Lab2.Assets.Scripts.Inventory;
using Lab2.Assets.Scripts.Player;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI.Inventory
{
    [RequireComponent(typeof(Image))]
    public class InventoryMenu : MonoBehaviour
    {
        public const float deadzone = 0.1f;

        [SerializeField]
        private InventoryGridController currentGrid;

        [SerializeField]
        private float timeRate;

        [SerializeField]
        private TMPro.TextMeshProUGUI typeText;

        [SerializeField]
        private PlayerInventory player;

        [SerializeField]
        private PlayerEquipment equipment;

        [SerializeField]
        private BoxFinderController boxFinder;

        public IInventory Player => player;
        public IInventory Equipment => equipment;
        public PlayerInventoryState CurrentState { get; private set; }
        public enum PlayerInventoryState
        {
            None,
            InBackpack,
            InLoot
        }

        public IInventory Other => CurrentState == PlayerInventoryState.InLoot ? boxFinder.LastBox : Equipment;

        public event EventHandler<OnSelectArg> OnSelect;
        public class OnSelectArg
        {
            public InventoryGridController currentGrid;
            public InventoryGridController swapGrid;
        }

        private bool UsingInventory => CurrentState != PlayerInventoryState.None;
        
        private Image _bg;
        private InventoryGridController swapGrid;
        private float _timestamp;

        private void Awake()
        {
            CurrentState = PlayerInventoryState.None;

            _bg = GetComponent<Image>();
            swapGrid = null;
            _timestamp = Time.time - timeRate;
        }

        private void Start()
        {
            DisplayUI(false);
        }

        private void Update()
        {
            if (boxFinder.OpeningBox && !UsingInventory)
            {
                CurrentState = PlayerInventoryState.InLoot;
            }

            if (Input.GetButtonDown("Start Function"))
            {
                switch (CurrentState)
                {
                    default:
                    case PlayerInventoryState.None:
                        CurrentState = PlayerInventoryState.InBackpack;
                        break;
                    case PlayerInventoryState.InBackpack:
                        CurrentState = PlayerInventoryState.None;
                        break;
                    case PlayerInventoryState.InLoot:
                        CurrentState = PlayerInventoryState.None;
                        break;
                }
            }

            DisplayUI(UsingInventory);
            ChangeText();

            if (UsingInventory)
            {
                if (Input.GetButtonDown("Swap Items"))
                {
                    TriggerSwapItems();
                }
                else if (Input.GetButtonDown("Drop Item"))
                {
                    TriggerDropItem();
                }
                else if (Input.GetButtonDown("Use Item"))
                {
                    //TODO
                }

                if (Time.time >= _timestamp + timeRate)
                {
                    if (Input.GetAxis("Menu Vertical") <= deadzone &&
                    Input.GetAxis("Menu Horizontal") <= deadzone)
                    {
                        ResetNavigationDelay();
                    }

                    if (Input.GetAxis("Menu Vertical") == 1) // Up
                    {
                        SelectNewGrid(currentGrid.NextGridUp);
                    }
                    else if (Input.GetAxis("Menu Horizontal") == 1) // Right
                    {
                        SelectNewGrid(currentGrid.NextGridRight);
                    }
                    else if (Input.GetAxis("Menu Vertical") == -1) // Down
                    {
                        SelectNewGrid(currentGrid.NextGridDown);
                    }
                    else if (Input.GetAxis("Menu Horizontal") == -1) // Left
                    {
                        SelectNewGrid(currentGrid.NextGridLeft);
                    }
                }
            }
        }

        private void DisplayUI(bool display)
        {
            _bg.enabled = display;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(display);
            }
            OnSelect?.Invoke(this, new OnSelectArg { currentGrid = currentGrid, swapGrid = swapGrid });
        }

        private void ChangeText()
        {
            switch (CurrentState)
            {
                default:
                case PlayerInventoryState.None:
                    typeText.text = "None";
                    break;
                case PlayerInventoryState.InBackpack:
                    typeText.text = "Equipment";
                    break;
                case PlayerInventoryState.InLoot:
                    typeText.text = "Chest";
                    break;
            }
        }

        private void ResetNavigationDelay()
        {
            _timestamp = Time.time - timeRate;
        }

        private void SelectNewGrid(InventoryGridController nextGrid)
        {
            if (nextGrid == null)
            {
                return;
            }

            currentGrid = nextGrid;
            _timestamp = Time.time;
            OnSelect?.Invoke(this, new OnSelectArg { currentGrid = currentGrid, swapGrid = swapGrid });
        }

        private void TriggerSwapItems()
        {
            if (swapGrid == null)
            {
                swapGrid = currentGrid;
            }
            else if (swapGrid != currentGrid)
            {
                InventoryContainer.SwapItems(swapGrid.InventoryIndex, currentGrid.InventoryIndex, swapGrid.CurrentInventory, currentGrid.CurrentInventory);

                swapGrid = null;
            }
            else
            {
                swapGrid = null;
            }

            OnSelect?.Invoke(this, new OnSelectArg { currentGrid = currentGrid, swapGrid = swapGrid });
        }

        private void TriggerDropItem()
        {
            swapGrid = null;

            InventoryContainer.Drop(currentGrid.InventoryIndex, currentGrid.CurrentContainer);

            OnSelect?.Invoke(this, new OnSelectArg { currentGrid = currentGrid, swapGrid = swapGrid });
        }
    }
}