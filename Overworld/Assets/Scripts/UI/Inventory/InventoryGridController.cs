﻿using Lab2.Assets.Scripts.Inventory;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI.Inventory
{
    [RequireComponent(typeof(Image))]
    public class InventoryGridController : MonoBehaviour
    {
        [SerializeField]
        private InventoryGridController nextGridUp;

        [SerializeField]
        private InventoryGridController nextGridRight;

        [SerializeField]
        private InventoryGridController nextGridDown;

        [SerializeField]
        private InventoryGridController nextGridLeft;

        [SerializeField]
        private Image _itemPreview;

        [SerializeField]
        private Image _armorRequiredImage;

        [SerializeField]
        private Image _weaponRequiredImage;

        [SerializeField]
        private int inventoryIndex;

        [SerializeField]
        private bool other;

        public IItem GridItem => CurrentContainer.HeldItems[inventoryIndex];
        public InventoryGridController NextGridUp => nextGridUp;
        public InventoryGridController NextGridRight => nextGridRight;
        public InventoryGridController NextGridDown => nextGridDown;
        public InventoryGridController NextGridLeft => nextGridLeft;
        public int InventoryIndex => inventoryIndex;
        public bool IsOther => other;

        public IInventory CurrentInventory => other ? _inventoryMenu.Other : _inventoryMenu.Player;
        public InventoryContainer CurrentContainer => CurrentInventory.Container;

        private Image _bg;
        private InventoryMenu _inventoryMenu;

        private void Awake()
        {
            _bg = GetComponent<Image>();
            _inventoryMenu = GetComponentInParent<InventoryMenu>();
        }

        private void OnEnable()
        {
            if (_inventoryMenu.Player.Container != null)
            {
                Subscribe();
                UpdatePreview();
            }            
        }

        private void OnDisable()
        {
            Unsubscribe();
        }

        private void Inventory_OnSelect(object sender, InventoryMenu.OnSelectArg e)
        {
            if (this == e.swapGrid)
            {
                _bg.color = Color.blue;
            }
            else if (this == e.currentGrid)
            {
                _bg.color = Color.red;
            }
            else
            {
                _bg.color = Color.white;
            }
        }

        private void Inventory_OnInventoryChange(object sender, InventoryContainer.InventoryChangeArgs e)
        {
            if (e.inventory == CurrentContainer)
            {
                UpdatePreview();
            }
        }

        private void Subscribe()
        {
            _inventoryMenu.OnSelect += Inventory_OnSelect;
            CurrentContainer.OnInventoryChange += Inventory_OnInventoryChange;
        }

        private void Unsubscribe()
        {
            _inventoryMenu.OnSelect -= Inventory_OnSelect;
            CurrentContainer.OnInventoryChange -= Inventory_OnInventoryChange;
        }

        private void UpdatePreview()
        {
            AbstractItem item = CurrentInventory.GetItemByIndex(InventoryIndex);
            if (item == null)
            {
                Type constraint = CurrentInventory.GetConstraint(InventoryIndex);

                _itemPreview.sprite = null;
                _armorRequiredImage.gameObject.SetActive(constraint == typeof(Armor));
                _weaponRequiredImage.gameObject.SetActive(constraint == typeof(Weapon));
            }
            else
            {
                _itemPreview.sprite = item.sprite;
                _armorRequiredImage.gameObject.SetActive(false);
                _weaponRequiredImage.gameObject.SetActive(false);
            }
        }
    }
}