﻿using Lab2.Assets.Scripts.Core;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI
{
    [RequireComponent(typeof(Image))]
    public class TransitionController : MonoBehaviour
    {
        private Image _image;

        private void Awake()
        {
            _image = GetComponent<Image>();
            _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 0);
        }

        private void OnEnable()
        {
            TransitionManager.Instance.OnTransition += Instance_OnTransition;
        }

        private void OnDisable()
        {
            TransitionManager.Instance.OnTransition -= Instance_OnTransition;
        }

        private void Instance_OnTransition(object sender, TransitionManager.TransitionArgs e)
        {
            switch (e.transitionState)
            {
                default:
                case TransitionManager.TransitionState.TransitionComplete:
                    _image.enabled = false;
                    break;
                case TransitionManager.TransitionState.StartingTransition:
                    _image.enabled = true;
                    StartCoroutine(Transition(0, 1));
                    break;
                case TransitionManager.TransitionState.EndingTransition:
                    StartCoroutine(Transition(1, 0));
                    break;
            }
        }

        private IEnumerator Transition(float startValue, float endValue)
        {
            float alpha = startValue;
            float startTimestamp = Time.time;

            while (alpha != endValue)
            {
                alpha = Mathf.Lerp(startValue, endValue, (Time.time - startTimestamp) / (TransitionManager.TRANSITION_TIME * 0.75f));
                _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, alpha);
                yield return null;
            }
        }
    }
}