﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI
{
    public class PauseController : MonoBehaviour
    {
        public bool Paused { get; private set; }

        private Image backgroundImage;
        private GameObject[] children;

        private void OnEnable()
        {
            backgroundImage = GetComponent<Image>();

            children = new GameObject[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
            {
                children[i] = transform.GetChild(i).gameObject;
            }

            Resume();
        }

        private void OnDisable()
        {
            Resume();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (Paused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }

        public void Pause()
        {
            Paused = true;
            Time.timeScale = 0;
            DisplayUI(Paused);
        }

        public void Resume()
        {
            Paused = false;
            Time.timeScale = 1;
            DisplayUI(Paused);
        }

        public void DisplayUI(bool display)
        {
            backgroundImage.enabled = display;
            foreach (GameObject child in children)
            {
                child.SetActive(display);
            }
        }

        public void QuitApplication()
        {
            Application.Quit();
        }

        public void SaveAndQuit()
        {
            Resume();
            StartCoroutine(GameManager.Instance.SaveAndQuit());
        }

        public void SaveAndReturnToTitle()
        {
            Resume();
            StartCoroutine(GameManager.Instance.SaveAndReturnToTitle());
        }

        public void Restart()
        {
            Resume();
            StartCoroutine(GameManager.Instance.Restart());
        }
    }
}