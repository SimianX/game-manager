﻿using TMPro;
using UnityEngine;

namespace Lab2.Assets.Scripts.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class GoldCounterController : MonoBehaviour
    {
        private const int DefaultValue = 0;
        private const int MaxGoldValue = 999;

        private TextMeshProUGUI _goldCountText;

        private void Awake()
        {
            _goldCountText = GetComponent<TextMeshProUGUI>();
            SetGoldValue(DefaultValue);
        }

        public void SetGoldValue(int goldValue)
        {
            if (goldValue > MaxGoldValue)
            {
                goldValue = MaxGoldValue;
            }
            else if (goldValue < 0)
            {
                goldValue = 0;
            }

            _goldCountText.text = goldValue.ToString("000");
        }
    }
}