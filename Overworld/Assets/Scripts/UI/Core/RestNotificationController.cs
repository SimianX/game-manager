﻿using Lab2.Assets.Scripts.Core;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI
{
    public class RestNotificationController : MonoBehaviour
    {
        [SerializeField]
        private Image backgroundImage;

        [SerializeField]
        private Image whiteImage;

        [SerializeField]
        private TextMeshProUGUI text;

        [SerializeField]
        private float holdTime;

        [SerializeField]
        private float fadeTime;

        private void Awake()
        {
            SetAlpha(0);
        }

        private void OnEnable()
        {
            GameManager.Instance.OnRest += Instance_OnRest;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnRest -= Instance_OnRest;
        }

        private void Instance_OnRest(object sender, System.EventArgs e)
        {
            StartCoroutine(Notify());
        }

        private IEnumerator Notify()
        {
            float alpha = 1;

            SetAlpha(alpha);

            yield return new WaitForSeconds(holdTime);

            float timestamp = Time.time;

            while (alpha != 0)
            {
                alpha = Mathf.Lerp(1, 0, (Time.time - timestamp) / fadeTime);

                SetAlpha(alpha);

                yield return null;
            }
        }

        private void SetAlpha(float alpha)
        {
            backgroundImage.color = new Color(backgroundImage.color.r, backgroundImage.color.g, backgroundImage.color.b, alpha);
            whiteImage.color = new Color(whiteImage.color.r, whiteImage.color.g, whiteImage.color.b, alpha);
            text.alpha = alpha;

            backgroundImage.enabled = alpha != 0;
            whiteImage.enabled = alpha != 0;
            text.enabled = alpha != 0;
        }
    }
}