﻿using Lab2.Assets.Scripts.Core;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI.Core
{
    [RequireComponent(typeof(Image))]
    public class AlertBannerController : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI message;

        [SerializeField]
        private float displayTime;

        private Image bg;

        private void Awake()
        {
            bg = GetComponent<Image>();

            Display(false);
        }

        private void OnEnable()
        {
            AlertManager.Instance.OnUserError += Instance_OnUserError;
        }

        private void OnDisable()
        {
            AlertManager.Instance.OnUserError -= Instance_OnUserError;
        }

        private void Instance_OnUserError(object sender, AlertManager.UserErrorArgs e)
        {
            message.text = e.message;

            StartCoroutine(Fade(displayTime));
        }

        private void Display(bool display)
        {
            bg.enabled = display;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(display);
            }
        }

        private IEnumerator Fade(float displayTime)
        {
            Display(true);

            yield return new WaitForSeconds(displayTime);

            Display(false);
        }
    }
}