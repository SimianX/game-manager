﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI
{
    public class MainMenuController : MenuController
    {
        [SerializeField]
        private Button _continueButton;

        public void Awake()
        {
            if (!GameManager.Instance.CanContinue)
            {
                _continueButton.interactable = false;
            }
        }

        public void Continue()
        {
            StartCoroutine(GameManager.Instance.Continue());
        }

        public void NewGame()
        {
            StartCoroutine(GameManager.Instance.NewGame());
        }
    }
}