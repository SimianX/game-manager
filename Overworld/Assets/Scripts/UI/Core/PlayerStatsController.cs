﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;

namespace Lab2.Assets.Scripts.UI
{
    public class PlayerStatsController : MonoBehaviour
    {
        [SerializeField]
        private HealthBarController healthBar;

        [SerializeField]
        private GoldCounterController goldCounter;

        private void OnEnable()
        {
            Player.Controller player = GetComponentInParent<Player.Controller>();

            if (player)
            {
                player.OnSet += Player_OnSet;
                player.OnDamage += Player_OnDamage;
                player.OnHeal += Player_OnHeal;
                player.OnGoldGained += Player_OnGoldGained;
                player.OnGoldLost += Player_OnGoldLost;
            }            
        }

        private void OnDisable()
        {
            Player.Controller player = GetComponentInParent<Player.Controller>();

            if (player)
            {
                player.OnSet -= Player_OnSet;
                player.OnDamage -= Player_OnDamage;
                player.OnHeal -= Player_OnHeal;
                player.OnGoldGained -= Player_OnGoldGained;
                player.OnGoldLost -= Player_OnGoldLost;
            }
        }

        private void Player_OnSet(object sender, Player.Controller.SetArgs e)
        {
            healthBar.SetCurrentHealthRatio(e.currentHealth / (e.maxHealth * 1.0f));
            goldCounter.SetGoldValue(e.heldGold);
        }

        private void Player_OnDamage(object sender, AbstractDamageable.DamageArgs e)
        {
            healthBar.SetCurrentHealthRatio(e.resultingHealth / (e.maxHealth * 1.0f));
        }

        private void Player_OnHeal(object sender, AbstractDamageable.HealArgs e)
        {
            healthBar.SetCurrentHealthRatio(e.resultingHealth / (e.maxHealth * 1.0f));
        }

        private void Player_OnGoldGained(object sender, Player.Controller.GoldGainedArgs e)
        {
            goldCounter.SetGoldValue(e.resultingAmount);
        }

        private void Player_OnGoldLost(object sender, Player.Controller.GoldLostArgs e)
        {
            goldCounter.SetGoldValue(e.resultingAmount);
        }
    }
}