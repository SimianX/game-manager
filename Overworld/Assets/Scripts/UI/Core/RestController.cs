﻿using Lab2.Assets.Scripts.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI
{
    [RequireComponent(typeof(Image))]
    public class RestController : MonoBehaviour
    {
        private string InfoText => "Pay " + rester.RestCost + "G to rest at inn?\n\nNOTE: Resting resets all enemies in the world";

        [SerializeField]
        private Player.Controller player;

        [SerializeField]
        private Player.RestController rester;

        [SerializeField]
        private TextMeshProUGUI infoText;

        [SerializeField]
        private Button restButton;

        private Image _image;

        private void Awake()
        {
            _image = GetComponent<Image>();
        }

        private void Update()
        {
            DisplayUI(rester.NearBed);
            infoText.text = InfoText;
            restButton.interactable = player.heldGold >= rester.RestCost;
        }

        public void RestAtInn()
        {
            player.HealDamage(player.MaxHealth);
            player.LoseGold(rester.RestCost);
            StartCoroutine(GameManager.Instance.ResetAllEnemies(true));
            rester.NearBed = false;
        }

        public void Cancel()
        {
            rester.NearBed = false;
        }

        private void DisplayUI(bool display)
        {
            _image.enabled = display;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(display);
            }
        }
    }
}