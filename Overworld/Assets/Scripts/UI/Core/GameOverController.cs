﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI
{
    public class GameOverController : MonoBehaviour
    {
        private Image _backgroundImage;

        private void Awake()
        {
            _backgroundImage = GetComponent<Image>();

            DisplayUI(false);
        }

        private void OnEnable()
        {
            Player.Controller player = GetComponentInParent<Player.Controller>();
            if (player)
            {
                player.OnDeath += Player_OnDeath;
            }
        }

        private void OnDisable()
        {
            Player.Controller player = GetComponentInParent<Player.Controller>();
            if (player)
            {
                player.OnDeath -= Player_OnDeath;
            }
        }

        private void Player_OnDeath(object sender, System.EventArgs e)
        {
            DisplayUI(true);
        }

        private void DisplayUI(bool display)
        {
            _backgroundImage.enabled = display;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(display);
            }
        }

        public void Restart()
        {
            DisplayUI(false);
            StartCoroutine(GameManager.Instance.Restart());
        }

        public void ReturnToTitle()
        {
            DisplayUI(false);
            TransitionManager.Instance.StartTransition(TransitionManager.MENU_SCENE_NAME);
        }

        public void Quit()
        {
            DisplayUI(false);
            Application.Quit();
        }
    }
}