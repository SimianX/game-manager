﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;

namespace Lab2.Assets.Scripts.UI
{
    public class MenuController : MonoBehaviour
    {
        public void QuitApplication()
        {
            Application.Quit();
        }

        public void SaveAndQuit()
        {
            StartCoroutine(GameManager.Instance.SaveAndQuit());
        }

        public void SaveAndReturnToTitle()
        {
            StartCoroutine(GameManager.Instance.SaveAndReturnToTitle());
        }

        public void Restart()
        {
            StartCoroutine(GameManager.Instance.Restart());
        }
    }
}