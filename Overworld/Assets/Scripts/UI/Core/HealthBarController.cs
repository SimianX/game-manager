﻿using UnityEngine;
using UnityEngine.UI;

namespace Lab2.Assets.Scripts.UI
{
    [RequireComponent(typeof(RectMask2D))]
    public class HealthBarController : MonoBehaviour
    {
        [SerializeField]
        private RawImage healthBarMax;

        [SerializeField]
        private RawImage healthBarCurrent;

        private RectMask2D _mask;

        private float HealthBarMaxDisplacement => healthBarMax.rectTransform.rect.width;

        private void Awake()
        {
            _mask = GetComponent<RectMask2D>();
        }

        public void SetCurrentHealthRatio(float currentHealthRatio)
        {
            if (float.IsNaN(currentHealthRatio))
            {
                currentHealthRatio = 0;
            }
            float displacement = -HealthBarMaxDisplacement + (HealthBarMaxDisplacement * currentHealthRatio);

            healthBarCurrent.transform.localPosition = new Vector3(displacement, 0, 0);
        }
    }
}