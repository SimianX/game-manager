﻿using Lab2.Assets.Scripts.Core;
using UnityEngine;

namespace Lab2.Assets.Scripts.UI
{
    public class EnemyStatsController : MonoBehaviour
    {
        [SerializeField]
        private EnemyController enemy;

        [SerializeField]
        private HealthBarController healthBar;

        private void Start()
        {
            healthBar.SetCurrentHealthRatio(enemy.CurrentHealth / (enemy.MaxHealth * 1.0f));
        }

        private void OnEnable()
        {
            enemy.OnDamage += Enemy_OnDamage;
            enemy.OnHeal += Enemy_OnHeal;
        }

        private void OnDisable()
        {
            enemy.OnDamage -= Enemy_OnDamage;
            enemy.OnHeal -= Enemy_OnHeal;
        }

        private void Enemy_OnDamage(object sender, AbstractDamageable.DamageArgs e)
        {
            healthBar.SetCurrentHealthRatio(e.resultingHealth / (e.maxHealth * 1.0f));
        }

        private void Enemy_OnHeal(object sender, AbstractDamageable.HealArgs e)
        {
            healthBar.SetCurrentHealthRatio(e.resultingHealth / (e.maxHealth * 1.0f));
        }

        //private void Update()
        //{
        //    transform.LookAt(UnityEngine.Camera.main.transform);
        //}
    }
}