﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Consumable", menuName = "Window/Item/Consumable")]
    public class Consumable : AbstractItem
    {
        public const string Type = "Consumable";

        public int health;

        public override string ItemType => Type;
        public override string UseText => "Consume";
        public override void Use()
        {
            throw new System.NotImplementedException();
        }
    }
}