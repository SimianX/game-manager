﻿using System.Collections.Generic;
using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    public abstract class AbstractInventory : MonoBehaviour, IInventory
    {
        public string key;

        [SerializeField]
        private int inventorySize;

        public AbstractItem[] defaultItems;

        [SerializeField]
        private AbstractInventory dropBox;

        public string Key => key;
        public InventoryContainer Container => InventoryManager.Instance.GetInventory(this);
        public int InventorySize => inventorySize;
        public AbstractItem[] DefaultItems => defaultItems;
        public string InventoryDataPath => InventoryContainer.INVENTORY_DATA_BASE_DIRECTORY + key + ".json";

        public abstract Dictionary<int, System.Type> SlotConstrainstsByIndex { get; }

        private void Start()
        {
            Container.OnItemDropped += Inventory_OnItemDropped;
        }

        private void OnDestroy()
        {
            Container.OnItemDropped -= Inventory_OnItemDropped;
        }

        public void DiableSaving()
        {
            Container.CanSave = false;
        }

        private void Inventory_OnItemDropped(object sender, InventoryContainer.ItemDroppedArgs e)
        {
            AbstractInventory newInventory = Instantiate(dropBox, transform.position, transform.rotation, null);
            InventoryContainer.Add(e.item, newInventory.Container);
            newInventory.DiableSaving();
        }
    }
}
