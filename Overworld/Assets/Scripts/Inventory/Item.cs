﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Item", menuName = "Window/Item/GenericItem")]
    public class Item : AbstractItem
    {
        public const string Type = "GenericItem";

        public override string ItemType => Type;
        public override string UseText => "Use";
        public override void Use()
        {
        }
    }
}