﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Armor", menuName = "Window/Item/Armor")]
    public class Armor : AbstractItem
    {
        public const string Type = "Armor";

        public int defence;

        public override string ItemType => Type;
        public override string UseText => "Equip";
        public override void Use()
        {
            throw new System.NotImplementedException();
        }
    }
}