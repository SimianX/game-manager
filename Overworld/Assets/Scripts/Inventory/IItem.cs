﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    public interface IItem
    {
        Sprite Sprite { get; }
        string ItemType { get; }
        string UseText { get; }
        void Use();
    }
}