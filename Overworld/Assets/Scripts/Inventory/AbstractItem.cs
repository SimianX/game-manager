﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    [System.Serializable]
    public abstract class AbstractItem : ScriptableObject, IItem
    {
        public new string name;
        public string description;
        public Sprite sprite;

        public Sprite Sprite => sprite;
        public abstract string ItemType { get; }
        public abstract string UseText { get; }
        public abstract void Use();
    }
}