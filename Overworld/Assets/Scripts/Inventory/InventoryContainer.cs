﻿using System;
using System.IO;
using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    [Serializable]
    public class InventoryContainer
    {
        public const string SAVE_DATA_BASE_DIRECTORY = "SaveFiles/";
        public const string INVENTORY_DATA_BASE_DIRECTORY = SAVE_DATA_BASE_DIRECTORY + "InventoryData/";

        public string ContainerSavePath { get; private set; }

        public bool CanSave { get; set; }

        [SerializeField]
        private AbstractItem[] _heldItems;

        public AbstractItem[] HeldItems
        {
            get { return _heldItems; }
            set { _heldItems = value; }
        }

        public event EventHandler<InventoryChangeArgs> OnInventoryChange;
        public class InventoryChangeArgs
        {
            public InventoryContainer inventory;
        }

        public event EventHandler<ItemDroppedArgs> OnItemDropped;
        public class ItemDroppedArgs
        {
            public AbstractItem item;
        }

        public InventoryContainer(IInventory inventory)
        {
            _heldItems = new AbstractItem[inventory.InventorySize];
            ContainerSavePath = inventory.InventoryDataPath;
            CanSave = true;

            InventoryContainer loadedInventory = Load(ContainerSavePath);
            if (loadedInventory == null)
            {
                for (int i = 0; i < HeldItems.Length; i++)
                {
                    if (i < inventory.DefaultItems.Length)
                    {
                        HeldItems[i] = inventory.DefaultItems[i];
                    }
                    else
                    {
                        HeldItems[i] = null;
                    }
                }
            }
            else
            {
                for(int i = 0; i < _heldItems.Length; i++)
                {
                    _heldItems[i] = loadedInventory.HeldItems[i];
                }
            }
        }

        public void UseItem(int itemIndex)
        {
            HeldItems[itemIndex].Use();

            OnInventoryChange?.Invoke(this, new InventoryChangeArgs() { inventory = this });
        }

        public static void SwapItems(int itemIndexA, int itemIndexB, IInventory inventoryA, IInventory inventoryB)
        {
            Type constraintA = inventoryA.GetConstraint(itemIndexA);
            Type constraintB = inventoryB.GetConstraint(itemIndexB);

            AbstractItem itemA = inventoryA.GetItemByIndex(itemIndexA);
            AbstractItem itemB = inventoryB.GetItemByIndex(itemIndexB);

            if (constraintA is object && itemB is object && itemB.GetType() != constraintA)
            {
                Core.AlertManager.Instance.LogUserError($"Can't swap because {inventoryB.GetItemByIndex(itemIndexB).name} isn't a {constraintA}");
                return;
            }

            if (constraintB is object && itemA is object && itemA.GetType() != constraintB)
            {
                Core.AlertManager.Instance.LogUserError($"Can't swap because {inventoryA.GetItemByIndex(itemIndexA).name} isn't a {constraintB}");
                return;
            }

            AbstractItem tempItem = inventoryA.Container.HeldItems[itemIndexA];
            inventoryA.Container.HeldItems[itemIndexA] = inventoryB.Container.HeldItems[itemIndexB];
            inventoryB.Container.HeldItems[itemIndexB] = tempItem;

            inventoryA.Container.Ping();
            inventoryB.Container.Ping();
        }

        public static void Drop(int itemIndex, InventoryContainer inventoryContainer)
        {
            AbstractItem droppedItem = inventoryContainer.HeldItems[itemIndex];
            inventoryContainer.HeldItems[itemIndex] = null;

            inventoryContainer.OnItemDropped?.Invoke(inventoryContainer, new ItemDroppedArgs() { item = droppedItem });
            inventoryContainer.Ping();
        }

        public static bool Add(AbstractItem item, InventoryContainer inventoryContainer)
        {
            for (int i = 0; i < inventoryContainer.HeldItems.Length; i++)
            {
                if (inventoryContainer.HeldItems[i] == null)
                {
                    inventoryContainer.HeldItems[i] = item;
                    return true;
                }
            }

            return false;
        }

        public void Ping()
        {
            OnInventoryChange?.Invoke(this, new InventoryChangeArgs() { inventory = this });
        }

        #region Serialization
        private static InventoryContainer Load(string path)
        {
            InventoryContainer inventoryContainer;

            try
            {
                ValidateFS();

                string json = File.ReadAllText(path);

                if (string.IsNullOrWhiteSpace(json))
                {
                    throw new Exception($"Inventory data file at ({path}) is null or whitespace");
                }

                inventoryContainer = JsonUtility.FromJson<InventoryContainer>(json);
            }
            catch
            {
                inventoryContainer = null;
            }

            return inventoryContainer;
        }

        public static void Save(InventoryContainer inventory)
        {
            if (inventory.CanSave == false)
            {
                return;
            }

            try
            {
                ValidateFS();

                File.WriteAllText(inventory.ContainerSavePath, JsonUtility.ToJson(inventory));
            }
            catch
            {
                Debug.LogError("Couldn't save Inventory!");
            }
        }

        public static void ValidateFS()
        {
            ValidateDirectory(SAVE_DATA_BASE_DIRECTORY);
            ValidateDirectory(INVENTORY_DATA_BASE_DIRECTORY);
        }

        private static void ValidateDirectory(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }
        #endregion
    }
}