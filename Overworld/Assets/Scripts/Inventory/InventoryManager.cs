﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    public class InventoryManager : MonoBehaviour
    {
        public static InventoryManager Instance { get; private set; }
        public bool Ready { get; private set; }

        public Dictionary<string, InventoryContainer> activeContainers;

        // Instantiate singleton and set defaults
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(this);
            }

            Ready = false;
        }

        // Get ready for initial load
        private void Start()
        {
            Core.GameManager.Instance.OnSave += GameManager_OnSave;
            Core.GameManager.Instance.OnNewGame += GameManager_OnNewGame;
            activeContainers = new Dictionary<string, InventoryContainer>();
            Ready = true;
        }

        public InventoryContainer GetInventory(IInventory inventory)
        {
            if (activeContainers.TryGetValue(inventory.Key, out InventoryContainer container))
            {
                return container;
            }
            else
            {
                activeContainers.Add(inventory.Key, container = new InventoryContainer(inventory));
                return container;
            }
        }

        private void GameManager_OnSave(object sender, System.EventArgs e)
        {
            SaveContainers(activeContainers);
        }

        private void GameManager_OnNewGame(object sender, System.EventArgs e)
        {
            InventoryContainer.ValidateFS();

            string[] paths = Directory.GetFiles(InventoryContainer.INVENTORY_DATA_BASE_DIRECTORY);
            foreach (string path in paths)
            {
                if (!string.IsNullOrEmpty(path))
                {
                    File.Delete(path);
                }
            }
        }

        private static void SaveContainers(Dictionary<string, InventoryContainer> containers)
        {
            foreach (InventoryContainer container in containers.Values)
            {
                InventoryContainer.Save(container);
            }
        }
    }
}