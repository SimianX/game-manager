﻿using System.Collections.Generic;

namespace Lab2.Assets.Scripts.Inventory
{
    public class PlayerEquipment : AbstractInventory
    {
        public override Dictionary<int, System.Type> SlotConstrainstsByIndex => new Dictionary<int, System.Type>()
        {
            {0, typeof(Weapon) },
            {1, typeof(Armor) },
            {2, typeof(Weapon) },
            {3, typeof(Armor) }
        };
    }
}