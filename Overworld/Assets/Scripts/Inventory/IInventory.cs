﻿using System;
using System.Collections.Generic;

namespace Lab2.Assets.Scripts.Inventory
{
    public interface IInventory
    {
        string Key { get; }
        InventoryContainer Container { get; }
        AbstractItem[] DefaultItems { get; }
        string InventoryDataPath { get; }
        int InventorySize { get; }
        Dictionary<int, Type> SlotConstrainstsByIndex { get; }
    }

    public static class InventoryExtensions
    {
        public static AbstractItem GetItemByIndex(this IInventory inventory, int itemIndex)
        {
            return inventory.Container.HeldItems[itemIndex];
        }

        public static Type GetConstraint(this IInventory inventory, int slotIndex)
        {
            if (inventory.SlotConstrainstsByIndex.TryGetValue(slotIndex, out Type constraint))
            {
                return constraint;
            }
            else
            {
                return null;
            }
        }
    }
}