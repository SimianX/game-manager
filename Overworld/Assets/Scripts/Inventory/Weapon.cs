﻿using UnityEngine;

namespace Lab2.Assets.Scripts.Inventory
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Weapon", menuName = "Window/Item/Weapon")]
    public class Weapon : AbstractItem
    {
        public const string Type = "Weapon";

        public int damage;

        public override string ItemType => Type;
        public override string UseText => "Equip";
        public override void Use()
        {
            throw new System.NotImplementedException();
        }
    }
}