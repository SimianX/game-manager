using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lab3
{
    public class HUDController : MonoBehaviour
    {
        [SerializeField]
        private Canvas canvas;

        public bool HUDDisplayed { get; private set; }

        private void Awake()
        {
            HUDDisplayed = true;
            Display(HUDDisplayed);
        }

        private void Update()
        {
            if (Input.GetButtonDown("Toggle HUD"))
            {
                HUDDisplayed = !HUDDisplayed;
                Display(HUDDisplayed);
            }
        }

        private void Display(bool display)
        {
            for (int i = 0; i < canvas.transform.childCount; i++)
            {
                canvas.transform.GetChild(i).gameObject.SetActive(display);
            }
        }
    }
}
